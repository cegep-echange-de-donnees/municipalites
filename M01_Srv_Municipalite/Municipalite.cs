﻿namespace M01_Srv_Municipalite
{
    public class Municipalite
    {
        public int CodeGeographique { get; private set; }
        public string Nom { get; private set; }
        public string Courriel { get; private set; }
        public string? SiteWeb { get; private set; }
        public DateTime? DateProchaineElection { get; private set; }
        public bool Actif { get; set; }
        public Municipalite(int p_CodeGeographique, string p_Nom, string p_Courriel, string? p_SiteWeb, DateTime? p_DateProchaineElection, bool p_Actif)
        {
            if(p_CodeGeographique < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_CodeGeographique));
            }
            if(p_Nom == null)
            {
                throw new ArgumentNullException(nameof(p_Nom));
            }
            if(p_Courriel == null)
            {
                throw new ArgumentNullException(nameof(p_Courriel));
            }
            this.CodeGeographique = p_CodeGeographique;
            this.Nom = p_Nom;
            this.Courriel = p_Courriel;
            this.SiteWeb = p_SiteWeb;
            this.DateProchaineElection = p_DateProchaineElection;
            this.Actif = p_Actif;
        }
        public override string ToString()
        {
            return $"Code Geographique: {this.CodeGeographique}\r\nNom: {this.Nom}\r\nCourriel: {this.Courriel}\r\n" +
                $"SiteWeb: {(this.SiteWeb == null ? "Aucun site web référencé" : this.SiteWeb)}\r\n" +
                $"Date prochaine election: {(this.DateProchaineElection == null ? "Aucune date d'élection connue" : this.DateProchaineElection.Value.Day + "/" + this.DateProchaineElection.Value.Month + "/" + this.DateProchaineElection.Value.Year)}\r\n";
        }
        public override bool Equals(object? obj)
        {
            if(obj is not Municipalite)
            {
                return false;
            }

            Municipalite autre = obj as Municipalite;
            return this.CodeGeographique == autre.CodeGeographique
                && this.Nom == autre.Nom
                && this.Courriel == autre.Courriel
                && this.SiteWeb == autre.SiteWeb
                && this.DateProchaineElection == autre.DateProchaineElection
                && this.Actif == autre.Actif;
        }
        public override int GetHashCode()
        {
            return this.GetHashCode();
        }
    }
}