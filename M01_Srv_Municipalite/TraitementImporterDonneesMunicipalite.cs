﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M01_Srv_Municipalite
{
    public class TraitementImporterDonneesMunicipalite
    {
        public IDepotImportationMunicipalites DepotImportationMunicipalites { get; private set; }
        public IDepotMunicipalites DepotMunicipalites { get; private set; }
        public TraitementImporterDonneesMunicipalite(IDepotImportationMunicipalites p_depotImportationMunicipalites, IDepotMunicipalites p_depotMunicipalites)
        {
            if (p_depotImportationMunicipalites == null)
            {
                throw new ArgumentNullException(nameof(p_depotImportationMunicipalites));
            }
            if (p_depotMunicipalites == null)
            {
                throw new ArgumentNullException(nameof(p_depotMunicipalites));
            }
            this.DepotImportationMunicipalites = p_depotImportationMunicipalites;
            this.DepotMunicipalites = p_depotMunicipalites;
        }
        public StatistiquesImportationDonnees Executer()
        {
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();
            Dictionary<int, Municipalite> municipalitesFichierDonnees = this.DepotImportationMunicipalites.LireMunicipalite().ToDictionary(municipalite => municipalite.CodeGeographique);

            if(municipalitesFichierDonnees == null)
            {
                municipalitesFichierDonnees = new Dictionary<int, M01_Srv_Municipalite.Municipalite>();
            }

            this.VerifierEnregistrementsAjoutesEtModifies(statistiquesImportationDonnees, municipalitesFichierDonnees);
            this.VerifierEnregistrementsDesactives(statistiquesImportationDonnees, municipalitesFichierDonnees);

            return statistiquesImportationDonnees;
        }

        private void VerifierEnregistrementsAjoutesEtModifies(StatistiquesImportationDonnees p_statistiquesImportationDonnees, Dictionary<int, Municipalite> p_municipalitesFichierDonnees)
        {
            foreach(KeyValuePair<int, Municipalite> municipaliteFichierDonnees in p_municipalitesFichierDonnees)
            {
                Municipalite municipalite = this.DepotMunicipalites.ChercherMunicipaliteParCodeGeographique(municipaliteFichierDonnees.Key);

                if(municipalite == null)
                {
                    this.DepotMunicipalites.AjouterMunicipalite(municipaliteFichierDonnees.Value);
                    p_statistiquesImportationDonnees.NombreEnregistrementsAjoutes++;
                }
                else if(!(municipalite.Equals(municipaliteFichierDonnees.Value)))
                {
                    this.DepotMunicipalites.MAJMunicipalite(municipaliteFichierDonnees.Value);
                    p_statistiquesImportationDonnees.NombreEnregistrementsModifies++;
                }
            }
        }
        private void VerifierEnregistrementsDesactives(StatistiquesImportationDonnees p_statistiquesImportationDonnees, Dictionary<int, Municipalite> p_municipalitesFichierDonnees)
        {
            Dictionary<int, Municipalite> municipalitesActives = this.DepotMunicipalites.ListerMunicipalitesActives().ToDictionary(municipalite => municipalite.CodeGeographique);

            foreach(KeyValuePair<int, Municipalite> municipaliteActive in municipalitesActives)
            {
                if(!(p_municipalitesFichierDonnees.ContainsKey(municipaliteActive.Key)))
                {
                    this.DepotMunicipalites.DesactiverMunicipalite(municipaliteActive.Value);
                    p_statistiquesImportationDonnees.NombreEnregistrementsDesactives++;
                }
            }
        }
    }
}
