﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M01_Srv_Municipalite
{
    public class StatistiquesImportationDonnees
    {
        public int NombreEnregistrementsAjoutes { get; set; }
        public int NombreEnregistrementsModifies { get; set; }
        public int NombreEnregistrementsDesactives { get; set; }
        public override string ToString()
        {
            return $"Nombre d'enregistrements ajoutés: {this.NombreEnregistrementsAjoutes}\r\n" +
                $"Nombre d'enregistrements modifiés: {this.NombreEnregistrementsModifies}\r\n" +
                $"Nombre d'enregistrements désactivés: {this.NombreEnregistrementsDesactives}";
        }
    }
}
