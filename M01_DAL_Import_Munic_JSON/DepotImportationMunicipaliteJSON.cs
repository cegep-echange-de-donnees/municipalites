﻿using M01_Srv_Municipalite;
using System.Text.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace M01_DAL_Import_Munic_JSON
{
    public class DepotImportationMunicipaliteJSON : IDepotImportationMunicipalites
    {
        public string Fichier { get; private set; }
        public DepotImportationMunicipaliteJSON(string p_fichier)
        {
            if (p_fichier == null || p_fichier.Length == 0)
            {
                throw new ArgumentNullException(nameof(p_fichier));
            }
            this.Fichier = p_fichier;
        }
        IEnumerable<M01_Srv_Municipalite.Municipalite> IDepotImportationMunicipalites.LireMunicipalite()
        {
            //List<Municipalite_Microsoft_Json> listeMunicipalitesJSONDTO = this.DeserialiserAvecMicrosoft();
            List<Municipalite_NewtonSoft_Json> listeMunicipalitesJSONDTO = this.DeserialiserAvecNewtonSoft();

            List<M01_Srv_Municipalite.Municipalite> listeMunicipalites = listeMunicipalitesJSONDTO.Select(municipaliteJSON => municipaliteJSON.VersEntite()).ToList();

            return listeMunicipalites;
        }
        private List<Municipalite_Microsoft_Json> DeserialiserAvecMicrosoft()
        {
            string chaineJSON = "";
            List<Municipalite_Microsoft_Json> listeMunicipalitesJSONDTO = new List<Municipalite_Microsoft_Json>();

            using (StreamReader sr = new StreamReader(this.Fichier))
            {
                chaineJSON = sr.ReadToEnd();
            }

            if(chaineJSON != null)
            {
                int debutChaine = chaineJSON.LastIndexOf('[');
                int longueurChaine = chaineJSON.LastIndexOf(']') + 1 - debutChaine;
                chaineJSON = chaineJSON.Substring(debutChaine, longueurChaine);

                if(chaineJSON != null)
                {
                    JsonSerializerOptions settings = new JsonSerializerOptions
                    {
                        IncludeFields = true
                    };

                    listeMunicipalitesJSONDTO = System.Text.Json.JsonSerializer.Deserialize<List<Municipalite_Microsoft_Json>>(chaineJSON, settings);

                    if (listeMunicipalitesJSONDTO == null)
                    {
                        listeMunicipalitesJSONDTO = new List<Municipalite_Microsoft_Json>();
                    }
                }
            }

            return listeMunicipalitesJSONDTO;
        }
        private List<Municipalite_NewtonSoft_Json> DeserialiserAvecNewtonSoft()
        {
            string chaineJSON = "";
            List<Municipalite_NewtonSoft_Json> listeMunicipalitesJSONDTO = new List<Municipalite_NewtonSoft_Json>();

            using (StreamReader sr = new StreamReader(this.Fichier))
            {
                chaineJSON = sr.ReadToEnd();
            }

            JObject chaine = JObject.Parse(chaineJSON);

            if(chaine != null)
            {
                List<JToken>? records = chaine?["result"]?["records"]?.Children().ToList();
                if(records != null && records.Count > 0)
                {
                    foreach (JToken record in records)
                    {
                        if (record != null)
                        {
                            Municipalite_NewtonSoft_Json? municipaliteDTO = record.ToObject<Municipalite_NewtonSoft_Json>();
                            if(municipaliteDTO != null)
                            {
                                listeMunicipalitesJSONDTO.Add(municipaliteDTO);
                            }
                        }
                    }
                }
            }

            return listeMunicipalitesJSONDTO;
        }
    }
}