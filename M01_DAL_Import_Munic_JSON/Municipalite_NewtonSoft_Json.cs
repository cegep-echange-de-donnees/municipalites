﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M01_DAL_Import_Munic_JSON
{
    public class Municipalite_NewtonSoft_Json
    {
        public int mcode { get; set; }
        public string munnom { get; set; }
        public string mcourriel { get; set; }
        public string? mweb { get; set; }
        public string datelec { get; set; }
        public Municipalite_NewtonSoft_Json()
        {
            ;
        }
        public M01_Srv_Municipalite.Municipalite VersEntite()
        {
            DateTime date;
            return new M01_Srv_Municipalite.Municipalite(this.mcode, this.munnom, this.mcourriel
                                                        , this.mweb == "" ? null : this.mweb
                                                        , DateTime.TryParse(this.datelec.Replace("-", "/"), out date) ? date.Date : (DateTime?)null
                                                        , true);
        }
    }
}
