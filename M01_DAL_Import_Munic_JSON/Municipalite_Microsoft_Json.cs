﻿using M01_Srv_Municipalite;
using System.Text.Json.Serialization;

namespace M01_DAL_Import_Munic_JSON
{
    public class Municipalite_Microsoft_Json
    {
        [JsonInclude]
        public string mcode { get; private set; }
        [JsonInclude]
        public string munnom { get; private set; }
        [JsonInclude]
        public string mcourriel { get; private set; }
        [JsonInclude]
        public string mweb { get; private set; }
        [JsonInclude]
        public string datelec { get; private set; }
        public Municipalite_Microsoft_Json()
        {
            ;
        }
        public M01_Srv_Municipalite.Municipalite VersEntite()
        {
            int codeGeographique = int.Parse(this.mcode);
            DateTime date;

            return new M01_Srv_Municipalite.Municipalite(codeGeographique, this.munnom, this.mcourriel
                                                        ,this.mweb == "" ? null : this.mweb
                                                        ,DateTime.TryParse(this.datelec.Replace("-", "/"), out date) ? date.Date : (DateTime?)null
                                                        ,true);
        }
    }
}