﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore.Internal;
using System.IO;

namespace M01_DAL_Municipalite_SQLServer
{
    public static class DALDbContextGeneration
    {
        private static DbContextOptions<SqlServerContext> _dbContextOptions;
        private static PooledDbContextFactory<SqlServerContext> _pooledDbContextFactory;

        static DALDbContextGeneration()
        {
            IConfigurationRoot configuration =
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.Parent?.FullName)
                    .AddJsonFile("appsettings.json", false)
                    .Build();

            _dbContextOptions = new DbContextOptionsBuilder<SqlServerContext>()
                    .UseSqlServer(configuration.GetConnectionString("DefaultConnection"))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
#if DEBUG
                    .LogTo(message => Debug.WriteLine(message), LogLevel.Information)
                    .EnableSensitiveDataLogging()
#endif
                    .Options;

            _pooledDbContextFactory = new PooledDbContextFactory<SqlServerContext>(_dbContextOptions);
        }
        public static SqlServerContext ObtenirApplicationDBContext()
        {
            return _pooledDbContextFactory.CreateDbContext();
        }
    }
}
