﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;


namespace M01_DAL_Municipalite_SQLServer
{
    public class SqlServerContext : DbContext
    {
        public DbSet<Municipalite> Municipalites { get; set; }
        public SqlServerContext(DbContextOptions p_dbContextOptions) : base(p_dbContextOptions)
        {
            ;
        }
    }
}