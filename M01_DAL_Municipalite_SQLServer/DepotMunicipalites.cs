﻿using M01_Srv_Municipalite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M01_DAL_Municipalite_SQLServer
{
    public class DepotMunicipalites : IDepotMunicipalites
    {
        private SqlServerContext m_SqlServerContext { get; set; }
        public DepotMunicipalites(SqlServerContext p_sqlServerContext)
        {
            if(p_sqlServerContext == null)
            {
                throw new ArgumentNullException(nameof(p_sqlServerContext));
            }
            m_SqlServerContext = p_sqlServerContext;
        }
        void IDepotMunicipalites.AjouterMunicipalite(M01_Srv_Municipalite.Municipalite p_municipalite)
        {
            Municipalite municipalite = new Municipalite(p_municipalite);
            this.m_SqlServerContext.Municipalites.Add(municipalite);
            this.m_SqlServerContext.SaveChanges();
            this.m_SqlServerContext.ChangeTracker.Clear();
        }

        M01_Srv_Municipalite.Municipalite IDepotMunicipalites.ChercherMunicipaliteParCodeGeographique(int p_codeGeographique)
        {
            return this.m_SqlServerContext.Municipalites.Where(municipalite => municipalite.MunicipaliteId == p_codeGeographique)
                                                        .Select(municipalite => municipalite.VersEntite()).FirstOrDefault();
        }

        void IDepotMunicipalites.DesactiverMunicipalite(M01_Srv_Municipalite.Municipalite p_municipalite)
        {
            p_municipalite.Actif = false;
            Municipalite municipalite = new Municipalite(p_municipalite);
            this.m_SqlServerContext.Municipalites.Update(municipalite);
            this.m_SqlServerContext.SaveChanges();
            this.m_SqlServerContext.ChangeTracker.Clear();
        }

        IEnumerable<M01_Srv_Municipalite.Municipalite> IDepotMunicipalites.ListerMunicipalitesActives()
        {
            IQueryable<Municipalite> requete = this.m_SqlServerContext.Municipalites;
            return requete.Where(municipalite => municipalite.Actif == true)
                          .Select(municipalite => municipalite.VersEntite()).ToList();
        }

        void IDepotMunicipalites.MAJMunicipalite(M01_Srv_Municipalite.Municipalite p_municipalite)
        {
            Municipalite municipalite = new Municipalite(p_municipalite);
            this.m_SqlServerContext.Municipalites.Update(municipalite);
            this.m_SqlServerContext.SaveChanges();
            this.m_SqlServerContext.ChangeTracker.Clear();
        }
    }
}
