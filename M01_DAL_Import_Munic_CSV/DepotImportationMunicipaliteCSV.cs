﻿using M01_Srv_Municipalite;

namespace M01_DAL_Import_Munic_CSV
{
    public class DepotImportationMunicipaliteCSV : IDepotImportationMunicipalites
    {
        public string Fichier { get; private set; }
        public DepotImportationMunicipaliteCSV(string p_fichier)
        {
            if(p_fichier == null || p_fichier.Length == 0)
            {
                throw new ArgumentNullException(nameof(p_fichier));
            }
            this.Fichier = p_fichier;
        }
        IEnumerable<Municipalite> IDepotImportationMunicipalites.LireMunicipalite()
        {
            List<Municipalite> listeMunicipalite = new List<Municipalite>();

            using(StreamReader sr = new StreamReader(this.Fichier))
            {
                List<string> champsMunicipalite = new List<string>();
                string? ligneMunicipalite = sr.ReadLine();

                if(ligneMunicipalite != null)
                {
                    this.Deserialiser(ligneMunicipalite, champsMunicipalite);

                    int indiceCodeGeographique = champsMunicipalite.IndexOf("mcode");
                    int indiceNom = champsMunicipalite.IndexOf("munnom");
                    int indiceCourriel = champsMunicipalite.IndexOf("mcourriel");
                    int indiceSiteWeb = champsMunicipalite.IndexOf("mweb");
                    int indiceDateElection = champsMunicipalite.IndexOf("datelec");

                    while ((ligneMunicipalite = sr.ReadLine()) != null)
                    {
                        this.Deserialiser(ligneMunicipalite, champsMunicipalite);

                        DateTime date;
                        Municipalite municipalite = new Municipalite(int.Parse(champsMunicipalite[indiceCodeGeographique]), champsMunicipalite[indiceNom], champsMunicipalite[indiceCourriel],
                            champsMunicipalite[indiceSiteWeb], DateTime.TryParse(champsMunicipalite[indiceDateElection].Replace("-", "/"), out date) ? date.Date : (DateTime?)null, true);
                        listeMunicipalite.Add(municipalite);
                    }
                }
            }

            return listeMunicipalite;
        }
        private void Deserialiser(string p_ligneMunicipalite, List<string> p_champsMunicipalite)
        {
            p_champsMunicipalite = p_ligneMunicipalite.Split("\",\"", StringSplitOptions.None).ToList();
            p_champsMunicipalite = p_champsMunicipalite.Select(champ => champ.Replace("\"", "")).ToList();
        }
    }
}