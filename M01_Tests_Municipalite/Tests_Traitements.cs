using Xunit;
using System;
using System.Collections.Generic;
using M01_Srv_Municipalite;
using System.Linq;
using FluentAssertions;
using M01_DAL_Municipalite_SQLServer;
using Moq;

namespace M01_Tests_Municipalite
{
    public class Tests_Traitements
    {
        #region Constructeurs
        [Fact]
        public void ctor_StatistiquesImportationDonnees_ObjetAttendu()
        {
            //Arranger & Agir
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Auditer
            statistiquesImportationDonnees.Should().NotBeNull();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void ctor_TraitementImporterDonneesMunicipalite_ParametresIDepotImportationMunicipalitesEstNul_ArgumentNullException()
        {
            //Arranger
            IDepotImportationMunicipalites depotImportationMunicipalites = null;
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            //Agir & Auditer
            Assert.Throws<ArgumentNullException>(() =>
            {
                TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(depotImportationMunicipalites, moqDepotMunicipalites.Object);
            });
        }
        [Fact]
        public void ctor_TraitementImporterDonneesMunicipalite_ParametresIDepotMunicipalitesEstNul_ArgumentNullException()
        {
            //Arranger
            IDepotMunicipalites depotMunicipalites = null;
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();

            //Agir & Auditer
            Assert.Throws<ArgumentNullException>(() =>
            {
                TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, depotMunicipalites);
            });
        }
        [Fact]
        public void ctor_TraitementImporterDonneesMunicipalite_ObjetAttendu()
        {
            //Arranger
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();

            //Agir
            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);

            //Auditer
            traitementImporterDonneesMunicipalite.Should().NotBeNull();
        }
        #endregion
        #region Ne fait rien
        [Fact]
        public void Executer_ZeroElementPresentDansDepotMunicipalite_ZeroElementAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>());
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>());

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_UnElementPresentDansDepotMunicipalite_ZeroElementAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_DixElementsPresentDansDepotMunicipalite_ZeroElementAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(2)).Returns(new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(3)).Returns(new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(4)).Returns(new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(5)).Returns(new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(6)).Returns(new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(7)).Returns(new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(8)).Returns(new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(9)).Returns(new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(10)).Returns(new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        #endregion
        #region Elements A Ajouter
        [Fact]
        public void Executer_ZeroElementPresentDansDepotMunicipalite_UnElementAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.AjouterMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(1);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_UnElementPresentDansDepotMunicipalite_UnElementAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(2));
            moqDepotMunicipalites.Verify(m => m.AjouterMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(1);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_NeufElementsPresentDansDepotMunicipalite_UnElementAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(2)).Returns(new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(3)).Returns(new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(4)).Returns(new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(5)).Returns(new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(6)).Returns(new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(7)).Returns(new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(8)).Returns(new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(9)).Returns(new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.AjouterMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(1);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_ZeroElementPresentDansDepotMunicipalite_10ElementsAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.AjouterMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(10);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_UnElementPresentDansDepotMunicipalite_NeufElementsAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.AjouterMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Exactly(9));
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(9);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_CinqElementsPresentsDansDepotMunicipalite_DixElementsAAjouter_ZeroElementAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(11, "La Malbaie", "villeDeLaMalbaie@villedelamalbaie.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(12, "Gatineau", "villeDeGatineau@villedegatineau.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(13, "Carleton-Sur-Mer", "villeDeCarletonSurMer@villedecarletonsurmer.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(14, "Mont-Tremblant", "villeDeMontTremblant@villedemonttremblant.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(15, "Laval", "villeDeLaval@villedelaval.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(2)).Returns(new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(3)).Returns(new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(4)).Returns(new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(5)).Returns(new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(11, "La Malbaie", "villeDeLaMalbaie@villedelamalbaie.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(12, "Gatineau", "villeDeGatineau@villedegatineau.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(13, "Carleton-Sur-Mer", "villeDeCarletonSurMer@villedecarletonsurmer.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(14, "Mont-Tremblant", "villeDeMontTremblant@villedemonttremblant.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(15, "Laval", "villeDeLaval@villedelaval.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(15));
            moqDepotMunicipalites.Verify(m => m.AjouterMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(10);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        #endregion
        #region Elements A Modifier
        [Fact]
        public void Executer_UnElementPresentDansDepotMunicipalite_ZeroElementAAjouter_UnElementAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.MAJMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(1);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_DixElementsPresentDansDepotMunicipalite_ZeroElementAAjouter_UnElementAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(2)).Returns(new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(3)).Returns(new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(4)).Returns(new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(5)).Returns(new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(6)).Returns(new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(7)).Returns(new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(8)).Returns(new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(9)).Returns(new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(10)).Returns(new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.MAJMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(1);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        [Fact]
        public void Executer_DixElementsPresentDansDepotMunicipalite_ZeroElementAAjouter_DixElementsAModifier_ZeroElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(2)).Returns(new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(3)).Returns(new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(4)).Returns(new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(5)).Returns(new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(6)).Returns(new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(7)).Returns(new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(8)).Returns(new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(9)).Returns(new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(10)).Returns(new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.MAJMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(10);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(0);
        }
        #endregion
        #region Elements A Desactiver
        [Fact]
        public void Executer_UnElementPresentDansDepotMunicipalite_ZeroElementAAjouter_ZeroElementAModifier_UnElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>());
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.DesactiverMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(1);
        }
        [Fact]
        public void Executer_DixElementsPresentDansDepotMunicipalite_ZeroElementAAjouter_ZeroElementAModifier_UnElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(2)).Returns(new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(3)).Returns(new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(4)).Returns(new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(5)).Returns(new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(6)).Returns(new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(7)).Returns(new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(8)).Returns(new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(9)).Returns(new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(9));
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.DesactiverMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(1);
        }
        [Fact]
        public void Executer_DixElementsPresentDansDepotMunicipalite_ZeroElementAAjouter_ZeroElementAModifier_DixElementsADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>());
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.DesactiverMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Exactly(10));
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(0);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(10);
        }
        #endregion
        #region Plusieurs Taches
        [Fact]
        public void Executer_DeuxElementsPresentDansDepotMunicipalite_UnElementAAjouter_UnElementAModifier_UnElementADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(2));
            moqDepotMunicipalites.Verify(m => m.AjouterMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.MAJMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.DesactiverMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Once());
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(1);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(1);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(1);
        }
        [Fact]
        public void Executer_DixElementsPresentDansDepotMunicipalite_CinqElementsAAjouter_CinqElementsAModifier_CinqElementsADesactiver()
        {
            //Arranger         
            Mock<IDepotImportationMunicipalites> moqDepotImportationMunicipalites = new Mock<IDepotImportationMunicipalites>();
            Mock<IDepotMunicipalites> moqDepotMunicipalites = new Mock<IDepotMunicipalites>();

            moqDepotImportationMunicipalites.Setup(m => m.LireMunicipalite()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(11, "La Malbaie", "villeDeLaMalbaie@villedelamalbaie.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(12, "Gatineau", "villeDeGatineau@villedegatineau.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(13, "Carleton-Sur-Mer", "villeDeCarletonSurMer@villedecarletonsurmer.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(14, "Mont-Tremblant", "villeDeMontTremblant@villedemonttremblant.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(15, "Laval", "villeDeLaval@villedelaval.qc.ca", null, null, true) }
            });
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(1)).Returns(new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(2)).Returns(new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(3)).Returns(new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(4)).Returns(new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ChercherMunicipaliteParCodeGeographique(5)).Returns(new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, false));
            moqDepotMunicipalites.Setup(m => m.ListerMunicipalitesActives()).Returns(new List<M01_Srv_Municipalite.Municipalite>()
            {
                {new M01_Srv_Municipalite.Municipalite(1, "Qu�bec", "villeDeQuebec@villedequebec.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(2, "Montreal", "villeDeMontreal@villedemontreal.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(3, "Baie-Saint-Paul", "villeDeBaieSaintPaul@villedebaiesaintpaul.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(4, "Chicoutimi", "villeDeChicoutimi@villedechicoutimi.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(5, "Saguenay", "villeDeSaguenay@villedesaguenay.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(6, "Tadoussac", "villeDeTadoussac@villedetadoussac.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(7, "Trois-Rivi�re", "villeDeTrois-Riviere@villedetroisriviere.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(8, "Gasp�", "villeDeGaspe@villedegaspe.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(9, "Levis", "villeDeLevis@villedelevis.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(10, "Shawinigan", "villeDeShawinigan@villedeshawinigan.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(11, "La Malbaie", "villeDeLaMalbaie@villedelamalbaie.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(12, "Gatineau", "villeDeGatineau@villedegatineau.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(13, "Carleton-Sur-Mer", "villeDeCarletonSurMer@villedecarletonsurmer.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(14, "Mont-Tremblant", "villeDeMontTremblant@villedemonttremblant.qc.ca", null, null, true) },
                {new M01_Srv_Municipalite.Municipalite(15, "Laval", "villeDeLaval@villedelaval.qc.ca", null, null, true) }
            });

            TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(moqDepotImportationMunicipalites.Object, moqDepotMunicipalites.Object);
            StatistiquesImportationDonnees statistiquesImportationDonnees = new StatistiquesImportationDonnees();

            //Agir
            statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();

            //Auditer
            moqDepotImportationMunicipalites.Verify(m => m.LireMunicipalite(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.ChercherMunicipaliteParCodeGeographique(It.IsAny<int>()), Times.Exactly(10));
            moqDepotMunicipalites.Verify(m => m.AjouterMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Exactly(5));
            moqDepotMunicipalites.Verify(m => m.MAJMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Exactly(5));
            moqDepotMunicipalites.Verify(m => m.ListerMunicipalitesActives(), Times.Once());
            moqDepotMunicipalites.Verify(m => m.DesactiverMunicipalite(It.IsAny<M01_Srv_Municipalite.Municipalite>()), Times.Exactly(5));
            moqDepotMunicipalites.VerifyNoOtherCalls();
            statistiquesImportationDonnees.NombreEnregistrementsAjoutes.Should().Be(5);
            statistiquesImportationDonnees.NombreEnregistrementsModifies.Should().Be(5);
            statistiquesImportationDonnees.NombreEnregistrementsDesactives.Should().Be(5);
        }
        #endregion
    }
}