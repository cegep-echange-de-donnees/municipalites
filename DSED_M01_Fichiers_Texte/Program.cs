﻿using M01_DAL_Import_Munic_CSV;
using M01_DAL_Import_Munic_JSON;
using M01_DAL_Import_Munic_REST_JSON;
using M01_DAL_Municipalite_SQLServer;
using M01_Srv_Municipalite;
using System.IO;

string? dossierSolution = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.Parent?.FullName;
string cheminAbsoluFichierCSV = dossierSolution + "\\" + "MUN.csv";
string cheminAbsoluFichierJSON = dossierSolution + "\\" + "MUN.json";

using (SqlServerContext sqlServerContext = DALDbContextGeneration.ObtenirApplicationDBContext())
{
    DepotMunicipalites depotMunicipalites = new DepotMunicipalites(sqlServerContext);
    IDepotImportationMunicipalites depotImportationMunicipalites;

    TraitementImporterDonneesMunicipalite traitementImporterDonneesMunicipalite;

    if (File.Exists(cheminAbsoluFichierCSV))
    {
        depotImportationMunicipalites = new DepotImportationMunicipaliteCSV(cheminAbsoluFichierCSV);
    }
    else if (File.Exists(cheminAbsoluFichierJSON))
    {
        depotImportationMunicipalites = new DepotImportationMunicipaliteJSON(cheminAbsoluFichierJSON);
    }
    else
    {
        depotImportationMunicipalites = new DepotImportationMunicipaliteREST(new HttpClient());
    }

    traitementImporterDonneesMunicipalite = new TraitementImporterDonneesMunicipalite(depotImportationMunicipalites, depotMunicipalites);
    StatistiquesImportationDonnees statistiquesImportationDonnees = traitementImporterDonneesMunicipalite.Executer();
    Console.Out.WriteLine(statistiquesImportationDonnees.ToString());
}
