﻿using M01_Srv_Municipalite;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;
using M01_DAL_Import_Munic_JSON;
using Newtonsoft.Json.Linq;

namespace M01_DAL_Import_Munic_REST_JSON
{
    public class DepotImportationMunicipaliteREST : IDepotImportationMunicipalites
    {
        private HttpClient m_httpClient;
        private static string uriRechercheParMunicipalite = "/recherche/api/action/datastore_search?resource_id=19385b4e-5503-4330-9e59-f998f5918363&limit=3000";
        public DepotImportationMunicipaliteREST(HttpClient p_httpClient)
        {
            if(p_httpClient == null)
            {
                throw new ArgumentNullException(nameof(p_httpClient));
            }
            this.m_httpClient = p_httpClient;
        }
        public IEnumerable<Municipalite> LireMunicipalite()
        {
            List<Municipalite_NewtonSoft_Json> municipaliteJSON = new List<Municipalite_NewtonSoft_Json>();
            List<M01_Srv_Municipalite.Municipalite> municipalites = null;

            m_httpClient.BaseAddress = new Uri("https://www.donneesquebec.ca");
            m_httpClient.DefaultRequestHeaders.Accept.Clear();
            m_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            Task<HttpResponseMessage> response = this.m_httpClient.GetAsync(uriRechercheParMunicipalite);

            if (response.Result.IsSuccessStatusCode)
            {
                Task<string> result = response.Result.Content.ReadAsStringAsync();
                result.Wait();
                string contenuJSON = result.Result;

                JObject chaine = JObject.Parse(contenuJSON);

                if (chaine != null)
                {
                    if(chaine["result"] != null)
                    {
                        if(chaine["result"]["records"] != null)
                        {
                            List<JToken> records = chaine["result"]["records"].Children().ToList();
                            if (records != null && records.Count > 0)
                            {
                                foreach (JToken record in records)
                                {
                                    if (record != null)
                                    {
                                        Municipalite_NewtonSoft_Json municipaliteDTO = record.ToObject<Municipalite_NewtonSoft_Json>();

                                        if(municipaliteDTO != null)
                                        {
                                            municipaliteJSON.Add(municipaliteDTO);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            municipalites = municipaliteJSON.Select(municipaliteJSON => municipaliteJSON.VersEntite()).ToList();

            return municipalites?? new List<Municipalite>();
        }
    }
}